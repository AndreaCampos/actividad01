<%-- 
    Document   : index
    Created on : 5 may 2021, 22:27:18
    Author     : AndreaCampos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="fmlogin" action="">
            <table >
                <!-- Titulo de la tabla-->
                <h1>Datos Alumno</h1>
                <!-- fila 1 de la tabla-->
                <tr >
                    <!--Aca es la celda de la etiqueta nombre-->
                    <td width="150" ><b>Nombre </b></td>
                    <td><input name="txtNombre" type="text"></td>
                </tr>
                <!--fila de espacio-->
                <tr>
                    <td height="10" colspan="1">&nbsp;</td>
                </tr>
                <!-- fila 2 de la tabla-->       
                <tr>
                    <!--Aca es la celda de la etiqueta Sección-->
                    <td><b>Sección </b></td>
                    <td><input name="txtSeccion" type="text"></td>
                </tr>
                <!--fila de espacio-->
                <tr>
                    <td height="25" colspan="2">&nbsp;</td>
                </tr>
                <!-- fila  de la tabla-->
                <tr>
                    <!--Aca es la celda de la etiqueta Boton-->
                    <td><input type="submit" value="Aceptar"></td>
                </tr>
            </table> 
        </form>
    </body>
</html>
